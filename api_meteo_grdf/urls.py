from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic import RedirectView
from station_meteo import views as meteo_views
from gaussmetre import urls
from gaussmetre import views as gauss_views

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^gaussmetre/', include(urls)),
    url(r'^add-weather-info/(?P<pk>[A-Za-z0-9]+)', meteo_views.AddWeatherInfo.as_view()),
    url(r'^get-weather-info/(?P<pk>[A-Za-z0-9]+)', meteo_views.GetWeatherInfo.as_view()),
    url(r'^get-weather-info-from/(?P<from>[0-9]+[-][0-9]+[-][0-9]+)/to/(?P<to>[0-9]+[-][0-9]+[-][0-9]+)$', meteo_views.GetWeatherInfoPeriod.as_view()),
    url(r'^get-weather-info-from/(?P<from>[0-9]+[-][0-9]+[-][0-9]+)/to/(?P<to>[0-9]+[-][0-9]+[-][0-9]+)/(?P<pk>[A-Za-z0-9]+)', meteo_views.GetWeatherInfoPeriodStation.as_view()),
    url(r'^get-gaussmetre-data/(?P<pk>[A-Za-z0-9]+)', gauss_views.GetGaussmetreRecord.as_view()),
    url(r'^$', RedirectView.as_view(url='/gaussmetre/upload/')), # Just for ease of use.
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

admin.site.site_header = 'Admin API meteo & gaussmetre'
