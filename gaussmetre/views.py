# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.shortcuts import render
from django.core.urlresolvers import reverse

from rest_framework import generics, status
from rest_framework.response import Response

from gaussmetre.serializers import GetGaussmetreRecordSerializer
from gaussmetre.models import Document, Station, GaussmetreRecord
from gaussmetre.forms import DocumentForm
from station_meteo.models import WeatherInfo
import csv
import os

def upload(request):
	# Handle file upload
	if request.method == 'POST':
		form = DocumentForm(request.POST, request.FILES)
		if form.is_valid():
			newdoc = Document(docfile = request.FILES['docfile'])
			newdoc.save()

			# get the station name from the HTML form
			station_name = request.POST.get('station_name')
			station_object = Station.objects.get(station_name = station_name)

			# only if a station name was specified in the <select>
			if station_name:
				# Catch name of file
				for filename, file in request.FILES.iteritems():
					name = request.FILES[filename].name
				# Change the path to file depending on your path
				# in dev /home/ben/api-meteo-grdf/api_meteo_grdf/gaussmetre/media/records/
				# in prod /opt/api-meteo-grdf/api-meteo-grdf/gaussmetre/media/records/
				with open('/opt/api-meteo-grdf/api-meteo-grdf/gaussmetre/media/records/'+name, 'rb') as csvfile:
					records_reader = csv.reader(csvfile, delimiter=',')
					for row in records_reader:
						# Check if cell in row contains "*"
						# If so, give it "None" value
						for i in range(len(row)):
							if row[i].find("*") != -1:
								row[i] = None
						record = GaussmetreRecord(related_station=station_object, timecode = row[0], gauss_value = row[1], latitude = row[2], longitude = row[3])
						record.save()
				#remove the uploaded csv file at the end
				os.remove('/opt/api-meteo-grdf/api-meteo-grdf/gaussmetre/media/records/'+name)
			else:
				return render_to_response(
				'gaussmetre/upload.html',
				{'station_names': station_name_list, 'form': form},
				context_instance=RequestContext(request)
	)

			# Redirect to upload ok template
			return render(request, 'gaussmetre/upload_ok.html')
	else:
		form = DocumentForm() # A empty, unbound form

	# select station name
	stations = Station.objects.all()
	station_name_list = []
	for station in stations:
		station_name_list.insert(0, station.station_name)

	# Render list page with the station list and the form
	return render_to_response(
		'gaussmetre/upload.html',
		{'station_names': station_name_list, 'form': form},
		context_instance=RequestContext(request)
	)

class GetGaussmetreRecord(generics.ListAPIView):	
	serializer_class = GetGaussmetreRecordSerializer
	lookup_url_kwarg = "pk"

	def get_queryset(self):
		# catch url argument
		gaussmetre = self.kwargs.get(self.lookup_url_kwarg)
		# select corresponding gaussmetre
		station_object = Station.objects.get(station_name=gaussmetre)
		# select all the records from this gaussmetre
		gauss_record = GaussmetreRecord.objects.filter(related_station=station_object)
		return gauss_record