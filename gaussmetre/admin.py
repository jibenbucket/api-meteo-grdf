from django.contrib import admin
from gaussmetre.models import Station, GaussmetreRecord, Document

class StationAdmin(admin.ModelAdmin):
	list_display   = ('station_name',)

class GaussmetreRecordAdmin(admin.ModelAdmin):
	list_display   = ('related_station', 'timecode', 'gauss_value', 'latitude', 'longitude',)
	list_filter    = ('related_station', )

   # Configuration du formulaire d'edition
	fieldsets = (
	   ('General', {
	        'fields': ('related_station', 'timecode', 'gauss_value', 'latitude', 'longitude')
	    }),
	)

class GaussmetreRecordDocumentAdmin(admin.ModelAdmin):
	list_display   = ('docfile',)

admin.site.register(Station, StationAdmin)
admin.site.register(GaussmetreRecord, GaussmetreRecordAdmin)
admin.site.register(Document, GaussmetreRecordDocumentAdmin)
