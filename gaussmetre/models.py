from django.db import models

class Station(models.Model):
	station_name = models.CharField(default="station_default_name", max_length=30, blank=False)

	def __str__(self):
		return self.station_name

class GaussmetreRecord(models.Model):
	related_station = models.ForeignKey(Station, related_name='related_station')
	timecode = models.IntegerField(blank=False, null=True)
	gauss_value = models.IntegerField(blank=False, null=True)
	latitude = models.FloatField(blank=False, null=True)
	longitude = models.FloatField(blank=False, null=True)

class Document(models.Model):
	docfile = models.FileField(upload_to='records')
