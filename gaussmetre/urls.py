# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from gaussmetre import views

urlpatterns = patterns('gaussmetre.views',
    url(r'^upload/$', 'upload', name='upload'),
)