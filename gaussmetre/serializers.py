
from rest_framework import exceptions, serializers
from gaussmetre.models import GaussmetreRecord 

class GetGaussmetreRecordSerializer(serializers.ModelSerializer):
    class Meta:
        model = GaussmetreRecord
        fields = ('related_station', 'timecode', 'gauss_value', 'latitude', 'longitude')
