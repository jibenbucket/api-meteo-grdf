from django.db import models
from django.utils.timezone import now
from datetime import datetime


class WeatherInfo(models.Model):
	created_at = models.DateTimeField(default=now, blank=True)
	station_name = models.CharField(default="station_default_name", max_length=20, blank=True)
	winddir = models.IntegerField(blank=False)
	windspeedmph = models.FloatField(blank=False)
	windgustmph = models.FloatField(blank=False)
	windgustdir = models.IntegerField(blank=False)
	windspdmph_avg2m = models.FloatField(blank=False)
	winddir_avg2m = models.IntegerField(blank=False)
	windgustmph_10m = models.FloatField(blank=False)
	windgustdir_10m = models.IntegerField(blank=False)
	humidity = models.FloatField(blank=False)
	tempf = models.FloatField(blank=False)
	rainin = models.FloatField(blank=False)
	dailyrainin = models.FloatField(blank=False)
	pressure = models.FloatField(blank=False)
	batt_lvl = models.FloatField(blank=False)
	light_lvl = models.FloatField(blank=False)