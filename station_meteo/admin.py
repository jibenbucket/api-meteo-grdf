from django.contrib import admin
from station_meteo.models import WeatherInfo

class WeatherAdmin(admin.ModelAdmin):
	list_display   = ('station_name', 'created_at', 'winddir', 'windspeedmph', 'windgustmph', 'windgustdir', 'windspdmph_avg2m', 'winddir_avg2m', 'windgustmph_10m', 'windgustdir_10m', 'humidity', 'tempf', 'rainin', 'dailyrainin', 'pressure', 'batt_lvl', 'light_lvl',)
	list_filter    = ('created_at', 'station_name', )

   # Configuration du formulaire d'edition
	fieldsets = (
	    # Fieldset 1 : meta-info (titre, auteur)
	   ('General', {
	        'fields': ('station_name', 'created_at', 'winddir', 'windspeedmph', 'windgustmph', 'windgustdir', 'windspdmph_avg2m', 'winddir_avg2m', 'windgustmph_10m', 'windgustdir_10m', 'humidity', 'tempf', 'rainin', 'dailyrainin', 'pressure', 'batt_lvl', 'light_lvl')
	    }),
	)
admin.site.register(WeatherInfo, WeatherAdmin)