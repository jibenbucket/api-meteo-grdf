from rest_framework import exceptions, serializers
from station_meteo.models import WeatherInfo 


class AddWeatherInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = WeatherInfo
        fields = ('created_at', 'winddir', 'windspeedmph', 'windgustmph', 'windgustdir', 'windspdmph_avg2m', 'winddir_avg2m', 'windgustmph_10m', 'windgustdir_10m', 'humidity', 'tempf', 'rainin', 'dailyrainin', 'pressure', 'batt_lvl', 'light_lvl')
        
class GetWeatherInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = WeatherInfo
        fields = ('station_name', 'created_at', 'winddir', 'windspeedmph', 'windgustmph', 'windgustdir', 'windspdmph_avg2m', 'winddir_avg2m', 'windgustmph_10m', 'windgustdir_10m', 'humidity', 'tempf', 'rainin', 'dailyrainin', 'pressure', 'batt_lvl', 'light_lvl')