from django.shortcuts import render
from station_meteo.models import WeatherInfo
from station_meteo.serializers import GetWeatherInfoSerializer, AddWeatherInfoSerializer
from rest_framework import generics

class AddWeatherInfo(generics.CreateAPIView):
	queryset = WeatherInfo.objects.all()
	serializer_class = AddWeatherInfoSerializer
	lookup_url_kwarg = "pk"
	
	def perform_create(self, serializer):
		station = self.kwargs.get(self.lookup_url_kwarg)
		print station
		serializer.save(station_name=station)

class GetWeatherInfo(generics.ListAPIView):	
	serializer_class = GetWeatherInfoSerializer
	lookup_url_kwarg = "pk"

	def get_queryset(self):
		station = self.kwargs.get(self.lookup_url_kwarg)
		station_infos = WeatherInfo.objects.filter(station_name=station)
		return station_infos

class GetWeatherInfoPeriod(generics.ListAPIView):
	serializer_class = GetWeatherInfoSerializer
	lookup_url_kwarg_from = "from"
	lookup_url_kwarg_to = "to"

	def get_queryset(self):
		kwarg_from = self.kwargs.get(self.lookup_url_kwarg_from)
		kwarg_to = self.kwargs.get(self.lookup_url_kwarg_to)
		station_infos = WeatherInfo.objects.filter(created_at__range=[kwarg_from, kwarg_to])
		return station_infos

class GetWeatherInfoPeriodStation(generics.ListAPIView):	
	serializer_class = GetWeatherInfoSerializer
	lookup_url_kwarg_from = "from"
	lookup_url_kwarg_to = "to"
	lookup_url_kwarg_station = "pk"

	def get_queryset(self):
		station = self.kwargs.get(self.lookup_url_kwarg_station)
		kwarg_from = self.kwargs.get(self.lookup_url_kwarg_from)
		kwarg_to = self.kwargs.get(self.lookup_url_kwarg_to)
		station_infos = WeatherInfo.objects.filter(station_name=station).filter(created_at__range=[kwarg_from, kwarg_to])
		return station_infos